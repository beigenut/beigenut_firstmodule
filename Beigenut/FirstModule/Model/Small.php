<?php


namespace Beigenut\FirstModule\Model;
use Beigenut\FirstModule\Api\Size;

class Small implements Size
{

    public function getSize()
    {
        return "Small";
    }

}