<?php


namespace Beigenut\FirstModule\Model;


class HeavyService
{
   public function __construct() {
       echo "HeavyService has been instantiated"."</br>";
   }

   public function printHeavyServerMessage(){
       echo "message from HeavyService class";
   }
}