<?php


namespace Beigenut\FirstModule\Model;
use Beigenut\FirstModule\Api\Size;

class Normal implements Size
{

    public function getSize()
    {
        return "Normal";
    }

}