<?php


namespace Beigenut\FirstModule\Model;
use Beigenut\FirstModule\Api\Size;

class Big implements Size
{

    public function getSize()
    {
        return "Big";
    }

}