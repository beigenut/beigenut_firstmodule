<?php


namespace Beigenut\FirstModule\Model;

use Beigenut\FirstModule\Api\Size;
use Beigenut\FirstModule\Api\Color;

class Book
{
    protected $color;
    protected $size;

    //  __construct() 안에 들어가는 아규먼트는 interface 를 constructor 에 inject 해준다
    //  React useEffect 처럼 initialize 해주는 기능이 __construct
    public function __construct(Color $color, Size $size) {
        // 베리어블 assign
        $this->color = $color;
        $this->size = $size;
    }
}