<?php


namespace Beigenut\FirstModule\Model;
use Beigenut\FirstModule\Api\Color;
use Beigenut\FirstModule\Api\Brightness;

class Red implements Color
{
    protected $brightness;
    public function __construct(Brightness $brightness) {
        $this->brightness = $brightness;
    }

    public function getColor()
    {
        return "Red";
    }

}