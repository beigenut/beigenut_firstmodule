<?php


namespace Beigenut\FirstModule\Model;
use Beigenut\FirstModule\Api\PencilInterface;
use Beigenut\FirstModule\Api\Size;
use Beigenut\FirstModule\Api\Color;

class Pencil implements PencilInterface
{
    protected $color;
    protected $size;
    protected $name;
    protected $school;

    //  __construct() 안에 들어가는 아규먼트는 interface
    //  React useEffect 처럼 initialize 해주는 기능이 __construct
    public function __construct(Color $color, Size $size, $name = null, $school = null) {
        $this->color = $color;
        $this->size = $size;
        $this->name = $name;
        $this->school = $school;
    }

    public function getPencilType()
    {
        return "pencil has ".$this->color->getColor()." color and ".$this->size->getSize()." size";
    }
}