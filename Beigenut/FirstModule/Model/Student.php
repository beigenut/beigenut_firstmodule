<?php


namespace Beigenut\FirstModule\Model;


class Student
{
    private $name;
    private $age;
    private $scores;

    public function __construct($name = "Alex", $age = 19,
                                array $scores=array('maths'=>92, "eng"=>39)) {
        $this->name = $name;
        $this->age = $age;
        $this->scores = $scores;
    }

}