<?php

namespace Beigenut\FirstModule\Plugin;

class PluginSolution
{

    // --- sortOrder 이해하기 ---
    public function beforeExecute(
      \Beigenut\FirstModule\Controller\Page\Helloworld $subject)
    {
        echo "before execute sort order 10"."</br>";
    }

    public function afterExecute(
      \Beigenut\FirstModule\Controller\Page\Helloworld $subject)
    {
        echo "after execute sort order 10"."</br>";
    }

    public function aroundExecute(
      \Beigenut\FirstModule\Controller\Page\Helloworld $subject, callable $proceed)
    {
        echo "before proceed sort order 10"."</br>";
        $proceed();
        echo "after proceed sort order 10"."</br>";
    }

    // --- Plugin 종류 이해하기 파트 계속 ---
    // 2nd 파라미터 $name 은 argument (다른 곳에서 받아오는)
    //    public function afterSetName(\Magento\Catalog\Model\Product $subject, $name)
    //    {
    //        return "Before Plugin ". $name;
    //    }

    // 2nd 파라미터는 매소드가 완료되고 나서의 결과값 일 뿐, 개념 차이 있음
    //    public function afterGetName(\Magento\Catalog\Model\Product $subject, $result)
    //    {
    //        return "Before Plugin". $result ." After Plugin";
    //    }

    // sku 라는 argument 를 받는 경우  Before process 1 After proceed
    //        public function aroundGetIdBySku(\Magento\Catalog\Model\Product $subject, callable $proceed, $sku)
    //        {
    //            echo "Before proceed"."</br>";
    //            $id = $proceed($sku);
    //            echo $id ."</br>";
    //            echo " After proceed";
    //            return $id;
    //        }

    // Before process Iphone 6 After proceed
    //    public function aroundGetName(\Magento\Catalog\Model\Product $subject, callable $proceed)
    //    {
    //        echo "Before proceed"."</br>";
    //        $name = $proceed;
    //        echo $name ."</br>";
    //        echo " After proceed";
    //        return $name;
    //    }

}