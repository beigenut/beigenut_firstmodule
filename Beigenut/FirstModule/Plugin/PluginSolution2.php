<?php


namespace Beigenut\FirstModule\Plugin;


class PluginSolution2
{
    // --- sortOrder 이해하기 ---
    public function beforeExecute(
      \Beigenut\FirstModule\Controller\Page\Helloworld $subject)
    {
        echo "before execute sort order 20"."</br>";
    }

    public function afterExecute(
      \Beigenut\FirstModule\Controller\Page\Helloworld $subject)
    {
        echo "after execute sort order 20"."</br>";
    }

    public function aroundExecute(
      \Beigenut\FirstModule\Controller\Page\Helloworld $subject, callable $proceed)
    {
        echo "before proceed sort order 20"."</br>";
        $proceed();
        echo "after proceed sort order 20"."</br>";
    }
}