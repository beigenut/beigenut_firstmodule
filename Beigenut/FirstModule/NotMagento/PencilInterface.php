<?php

namespace Beigenut\FirstModule\NotMagento;

interface PencilInterface
{
    public function getPencilType();
}