<?php


namespace Beigenut\FirstModule\NotMagento;


class BigPencil implements PencilInterface
{

    public function getPencilType()
    {
        return "Big pencil";
    }

}