<?php

namespace Beigenut\FirstModule\Api;

interface PencilInterface
{
    public function getPencilType();
}