<?php


namespace Beigenut\FirstModule\Api;


interface Color
{
    public function getColor();
}