<?php


namespace Beigenut\FirstModule\Api;


interface Size
{
    public function getSize();
}