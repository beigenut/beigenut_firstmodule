<?php

namespace Beigenut\FirstModule\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class HelloWorld extends Command
{

    const CUSTOMERNAME = 'Name';

    public function configure()
    {
        $commandoptions = [
          new InputOption(self::CUSTOMERNAME, null, InputOption::VALUE_REQUIRED,
            'Name'),
        ];
        $this->setName('training:hello_world');
        $this->setDescription('the command prints out hello world')
          ->setAliases(['hw']);
        $this->setDefinition($commandoptions);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        if ($customername = $input->getOption(self::CUSTOMERNAME)) {
            $output->writeln("Hi " . $customername);
        } else {
            $output->writeln("Hi Customer, add -Name='smt' next time ");

        }

    }

}