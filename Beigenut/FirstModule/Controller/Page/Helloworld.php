<?php namespace Beigenut\FirstModule\Controller\Page;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Beigenut\FirstModule\Api\PencilInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Beigenut\FirstModule\Model\PencilFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\App\Request\Http;
use Beigenut\FirstModule\Model\HeavyService;

class Helloworld extends \Magento\Framework\App\Action\Action
{

    protected $pencilInterface;

    protected $productRepository;

    protected $pencilFactory;

    protected $_eventManager;

    protected $http;

    protected $heavyService;

    public function __construct(
      HeavyService $heavyService,
      Http $http,
      ManagerInterface $_eventManager,
      Context $context,
      ProductFactory $productFactory,
      PencilFactory $pencilFactory,
      PencilInterface $pencilInterface,
      ProductRepositoryInterface $productRepository
    ) {
        $this->_eventManager     = $_eventManager;
        $this->productFactory    = $productFactory;
        $this->pencilFactory     = $pencilFactory;
        $this->pencilInterface   = $pencilInterface;
        $this->productRepository = $productRepository;
        $this->http              = $http;
        $this->heavyService      = $heavyService;
        parent::__construct($context);
    }

    public function execute()
    {
        //    echo get_class($this->productRepository);
        //    echo $this->pencilInterface->getPencilType();

        //    $book = $objectManager->create('Beigenut\FirstModule\Model\Book');
        //    var_dump($book);

        //    $student = $objectManager->create('Beigenut\FirstModule\Model\Student');
        //    var_dump($student);

        //    $pencil = $objectManager->create("\Beigenut\FirstModule\Model\Pencil");
        //    var_dump($pencil);

        //        $pencil = $this->pencilFactory->create(array("name" => "Alex", "school"=>"International college"));
        //        var_dump($pencil);

        // --- Plugin 종류 이해하기 파트 계속 ---
        //        $product = $this->productFactory->create()->load(1);
        //        $product->setName("Iphone 6");
        //        $productName = $product->getName();
        //        $productName = $product->getIdBySku("A00001");
        //        echo $productName;

        // --- sortOrder 이해하기 파트 계속 ---
        //        echo "Main function"."</br>";

        //       --- Event & Observer 이해하기 파트 계속 ---
        //        $message = new \Magento\Framework\DataObject(["greeting" => "Good afternoon"]);
        //        $this->_eventManager->dispatch('custom_event', ['greeting'=>$message]);
        //        echo $message->getGreeting();

        // Proxy 이해하기 파트 ---
        $id = $this->http->getParam('id', 0);
        if ($id == 1) {
            $this->heavyService->printHeavyServerMessage();
        } else {
            echo "HeavyService not used";
        }
    }

}