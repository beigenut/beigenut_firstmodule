<?php

namespace Beigenut\Database\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    //  addColumn(
    //      'entity_id',  primary => true 를 주면 자동 indexing id 가 됨
    //      Table::TYPE_INTEGER,   데이터 타입
    //      null,   Data length
    //      ['identity' => true, 'nullable' => false, 'primary' => true],   데이터 속성
    //      'MEMBER ID'   해당 속성의 설명. phpmyadmin 보면 속성 하단에 써있는 설명
    //  )

    public function install(
      SchemaSetupInterface $setup,
      ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $table = $setup->getConnection()->newTable(
          $setup->getTable('affiliate_member')
        )->addColumn(
          'entity_id',
          Table::TYPE_INTEGER,
          null,
          ['identity' => true, 'nullable' => false, 'primary' => true],
          'MEMBER ID'
        )->addColumn(
          'name',
          Table::TYPE_TEXT,
          255,
          ['nullable' => false],
          'NAME OF MEMBER'
        )->addColumn(
          'address',
          Table::TYPE_TEXT,
          255,
          ['nullable' => false],
          'ADDRESS OF MEMBER'
        )->addColumn(
          'status',
          Table::TYPE_BOOLEAN,
          10,
          ['nullable' => false, 'default' => false],
          'STATUS'
        )->addColumn(
          'created_at',
          Table::TYPE_TIMESTAMP,
          null,
          ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
          'TIME CREATED'
        )->addColumn(
          'updated_at',
          Table::TYPE_TIMESTAMP,
          null,
          ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
          'TIME FOR UPDATE'
        )->setComment(
          'Affiliate Member table'

        );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }

}