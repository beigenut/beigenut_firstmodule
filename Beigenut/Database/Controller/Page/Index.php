<?php


namespace Beigenut\Database\Controller\Page;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Beigenut\Database\Model\AffiliateMemberFactory;
use Beigenut\Database\Model\ResourceModel\AffiliateMember;

class Index extends Action
{

    protected $affiliateMemberFactory;

    public function __construct(
      Context $context,
      AffiliateMemberFactory $affiliateMemberFactory
    ) {
        $this->affiliateMemberFactory = $affiliateMemberFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        // id = 1 인 맴버 정보 불러오기
        $affiliateMember = $this->affiliateMemberFactory->create();
        //        $member          = $affiliateMember->load(1);
        //        $member->setAddress('new address');
        //        $member->save();

        //        var_dump($member->getData());

        //        $affiliateMember->addData(['name'=>'Rand', 'address'=>'No 100, Dubai', 'status'=>true, 'phone_number'=>'1231231111']);
        //        $affiliateMember->save();

        //        --- Collection ---
        //        $collection = $affiliateMember->getCollection()
        //        모든 데이터 컬럼이 다 나오는게 아니라 특정 컬럼 만 나오게 하고 싶다면
        $collection = $affiliateMember->getCollection()
          ->addFieldToSelect(['name', 'status'])
          ->addFieldToFilter('name', ['eq' => 'bob']);
        //        boolean 타입 필터 적용은
        //        ->addFieldToFilter('status', array('neq'=>true));

        foreach ($collection as $item) {
            print_r($item->getData());
            echo '</br>';
        }
    }

}